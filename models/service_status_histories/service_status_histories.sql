with serviceStatus as (

	select _id,service,customerId, mobile,email,branchName,imei,smsLink,item_object from (SELECT _id,service,customerId, mobile,email,branchName,imei,smsLink, arr.position,arr.item_object FROM  service_status_histories,
	jsonb_array_elements(statusHistory) with ordinality arr(item_object, position)) as a 
),


inter_serviceStatus as (
		
	select serviceStatus._id as _id,
    serviceStatus.service,
    serviceStatus.customerId,
	serviceStatus.mobile,
    serviceStatus.email,
    serviceStatus.branchName,
    serviceStatus.imei,
    serviceStatus.smsLink,
           serviceStatus.item_object->'statusHistory'->'status' as status,
           serviceStatus.item_object->'statusHistory'->'currentStatus' as currentStatus,
           serviceStatus.item_object->'statusHistory'->'imgUrl' as imgUrl,
           serviceStatus.item_object->'statusHistory'->'updateBy' as updateBy,
           serviceStatus.item_object->'statusHistory'->'date' as actionDate
    from serviceStatus

),
final as (

    select _id,
    	   trim('"' from service::text) as service,
    	   trim('"' from customerId::text) as customerId,
    	   trim('"' from mobile::text) as mobile,
    	   trim('"' from email::text) as email,
    	   trim('"' from branchName::text) as branchName,
    	   trim('"' from imei::text) as imei,
            trim('"' from smsLink::text) as smsLink,
             trim('"' from status::text) as status,
    	   trim('"' from currentStatus::text) as currentStatus,
    	   trim('"' from imgUrl::text) as imgUrl,
           trim('"' from updateBy::text) as updateBy,
           actionDate::text::timestamp as actionDate
    from inter_serviceStatus
)

select * from final